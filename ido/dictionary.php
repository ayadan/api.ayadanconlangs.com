<?php
header("Content-Type:application/json");

/* API Response ----------------------------------------------------- */
function Response( $status, $status_message, $data )
{
    header("HTTP/1.1 ".$status);
    
    $response['status']=$status;
    $response['status_message']=$status_message;
    $response['data']=$data;
    
    $json_response = json_encode($response);
    
    echo $json_response;
}
/* ------------------------------------------------------------------ */

/* API Request listener --------------------------------------------- */
if ( isset( $_GET["search"] ) )
{
    Search( Prepare( $_GET ) );
}
/* ------------------------------------------------------------------ */
/* ------------------------------------------------------------------ */
/* ------------------------------------------------------------------ */

/**
Prepare the parameter list, set default values
*/
function Prepare( $get )
{
    $params = array();
    
    if ( isset( $get["lookat"] ) )      { $params["lookat"] = $get["lookat"]; }
    else                                { $params["lookat"] = "all"; }
    
    if ( isset( $get["startswith"] ) )  { $params["startswith"] = $get["startswith"]; }
    else                                { $params["startswith"] = false; }
    
    if ( isset( $get["wholeword"] ) )   { $params["wholeword"] = $get["startswith"]; }
    else                                { $params["wholeword"] = false; }
    
    return $params;
}

/**
@param  $params["search"]       Search term
@param  $params["lookat"]       Which field to inspect for the search term
@param  $params["startswith"]   Whether to search based on what the field starts with
@param  $params["wholeword"]    Whether to match based on if the whole word is found (e.g., "cat" but not "conCATenate")
*/
function Search( $params )
{
}

?>
