<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> APIs - api.ayadanconlangs.com </title>
  <meta name="description" content="Computer-readable conlang data for use to build APIs and other products">
  <meta name="author" content="Rachel Singh">

  <link rel="stylesheet" href="basic.css">

</head>

<body>
    <h1> API @ ayadanconlangs.com </h1>
    
    <p><a href="http://api.ayadanconlangs.com">API</a> | <a href="http://data.ayadanconlangs.com">Data</a> | <a href="http://tools.ayadanconlangs.com">Tools</a> | <a href="http://www.ayadanconlangs.com">Main website</a></p>
    
    <p>
        This page provides documentation about the APIs provided
        through ayadanconlangs.com.
    </p>
    
    <p><strong>Repository:</strong> <a href="https://bitbucket.org/ayadan/api.ayadanconlangs.com">https://bitbucket.org/ayadan/api.ayadanconlangs.com</a></p>
    
    <hr>
    
    <h2>Esperanto Dictionary</h2>
    
    <hr>
    
    <h2>Ido Dictionary</h2>
    
    <hr>
    
    <h2>Láadan Dictionary</h2>
    
    
    
    
</body>
</html>
